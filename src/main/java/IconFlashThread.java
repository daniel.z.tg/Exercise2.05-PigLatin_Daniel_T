import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import java.awt.Color;
import java.awt.image.BufferedImage;

import me.danielzgtg.compsci11_sem2_2017.common.TickTimer;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.ui.GUIJImagePanel;
import me.danielzgtg.compsci11_sem2_2017.common.ui.GraphicsUtils;

/**
 * A thread that flashes the "activity" icon if when the user starts typing,
 * and some time after.
 *
 * @author Daniel Tang
 * @since 23 April 2017
 */
/*packaged*/ class IconFlashThread extends Thread {

	/**
	 * The list of colors to tint the flashing activity icon.
	 */
	private static final List<Color> FLASH_COLORS;

	static {
		@SuppressWarnings("rawtypes")
		final List colorIntList = (List) PigLatin.UI_DATA.get(
				"gui.flashcolors");
		final List<Color> colorList = new LinkedList<>();
		for (Object o : colorIntList) {
			if (!(o instanceof Long)) {
				throw new IllegalArgumentException();
			}

			colorList.add(new Color((int) (long) o));
		}

		//noinspection unchecked
		FLASH_COLORS = Collections.unmodifiableList(colorList);
	}

	/**
	 * The number of flashes, that the activity icon will flash after the last detected typing.
	 */
	private static final int NUM_FLASHES = (int) (long) PigLatin.UI_DATA.get(
			"gui.flashes");

	/**
	 * The wait time between flashes, in milliseconds.
	 */
	private static final long FLASH_DELAY = (long) PigLatin.UI_DATA.get(
			"gui.flashdelay");

	/**
	 * The {@code long} reference containing the last update time, updated on GUI events.
	 */
	private final /*mutable*/ AtomicLong lastActivityTimeRef;

	/**
	 * The activity {@link GUIJImagePanel} label itself.
	 */
	private final /*mutable*/ GUIJImagePanel target;

	/**
	 * All of the possible {@link BufferedImage}s to use as flashing icons.
	 *
	 * Index 0 is always the default, and inactive icon.
	 */
	private final /*immutable*/ BufferedImage[] activityIcons;

	/**
	 * The timer that will help wait between flashes.
	 */
	private final TickTimer timer = new TickTimer(FLASH_DELAY * TickTimer.NANOSECONDS_MILLISECONDS_CONV);

	/**
	 * Whether this thread should continue.
	 */
	private volatile boolean running = true;

	/**
	 * Creates a new {@link IconFlashThread}.
	 *
	 * @param lastActivityTimeRef The {@code long} containing the last update time, updated on GUI events.
	 * @param target The activity {@link GUIJImagePanel} label itself.
	 * @param activityIconBase The default, and inactive {@link BufferedImage} icon.
	 */
	/*packaged*/ IconFlashThread(final AtomicLong lastActivityTimeRef, final GUIJImagePanel target,
			final BufferedImage activityIconBase) {
		Validate.notNull(lastActivityTimeRef, "Need the last update time variable!");
		Validate.notNull(target, "Must have a target!");

		this.lastActivityTimeRef = lastActivityTimeRef;
		this.target = target;
		this.activityIcons = new BufferedImage[FLASH_COLORS.size() + 1];

		this.activityIcons[0] = activityIconBase; // index 0 must be the default and inactive icon
		//{
		//activityIconBase,
		// Populate list with many colors (doesn't matter which)
		//GraphicsUtils.asTintedImage(activityIconBase, new Color(0xFF_55_44)),
		//GraphicsUtils.asTintedImage(activityIconBase, new Color(0xCC_44_FF)),
		//GraphicsUtils.asTintedImage(activityIconBase, new Color(0x00_AA_FF)),
		//GraphicsUtils.asTintedImage(activityIconBase, new Color(0xAA_FF_00)),
		//GraphicsUtils.asTintedImage(activityIconBase, new Color(0xFF_DD_00)),
		//GraphicsUtils.asTintedImage(activityIconBase, new Color(0xFF_99_00))
		//};

		// Load colors and tint
		final int numFlashColors = FLASH_COLORS.size();
		for (int i = 0; i < numFlashColors; i++) {
			this.activityIcons[i + 1] = GraphicsUtils.asTintedImage(activityIconBase, FLASH_COLORS.get(i));
		}

		// Background thread options
		this.setDaemon(true);
		this.setName("Pig Latin Icon Flash Thread");
	}

	@Override
	public void run() {
		long lastActivityTime = lastActivityTimeRef.get();
		int flashes = NUM_FLASHES;
		int activityIconIndex = 0;

		timer.reset();
		while (running) {
			// We start flashing again when translation is detected.
			final long newLastActivityTime = lastActivityTimeRef.get();

			if (lastActivityTime != newLastActivityTime) {
				lastActivityTime = newLastActivityTime;
				flashes = 0;
			}

			if (flashes < NUM_FLASHES) {
				// Cycle through images when flashing.
				target.setImage(activityIcons[activityIconIndex =
						(activityIconIndex + 1) % activityIcons.length]);
				flashes++;
			} else {
				// After done flashing, show inactive icon.
				target.setImage(activityIcons[activityIconIndex = 0]);
			}

			timer.waitUntilNextTick();
		}
	}

	/**
	 * Requests that the loop exit, and the thread stop.
	 */
	/*packaged*/ void shutdown() {
		this.running = false;
	}
}
