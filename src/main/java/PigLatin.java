import java.util.Collections;
import java.util.Map;
import java.util.regex.Matcher;

import java.io.InputStream;

import me.danielzgtg.compsci11_sem2_2017.common.platform.ConfigUtil;
import me.danielzgtg.compsci11_sem2_2017.common.platform.ResourceUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.LatinLanguageUtils;

/**
 * A translator for normal Latin-type language to Pig Latin.
 *
 * @author Daniel Tang
 * @since 12 April 2017
 */
@SuppressWarnings({"WeakerAccess", "unchecked"})
public final class PigLatin {

	/**
	 * Config for user interfaces
	 */
	@SuppressWarnings("rawtypes")
	public static final Map UI_DATA;

	static {
		// Populate ui data
		final InputStream uiStringsStream =
				ResourceUtils.getBundledResourceStream("uidata.config", PigLatin.class);

		try {
			UI_DATA = Collections.unmodifiableMap(ConfigUtil.parse(uiStringsStream));
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Determines if a letter might be a vowel
	 *
	 * @param letter The char to check
	 * @return {@code true} is the char is indeed a voxel
	 */
	public static final boolean isVowel(final char letter) {
		return LatinLanguageUtils.mightBeEnglishVowel(letter);
	}

	/**
	 * Finds the point a word should be chopped up for this
	 * particular dialect of Pig Latin. Does not actually find
	 * first vowel literally (thus internal use only).
	 *
	 * @param word The word to find the chop point
	 * @return The character index of the chop point, or -1 if it could not be found
	 */
	private static final int vowelFinder(final String word) {
		final char[] letters = word.toCharArray();

		boolean wasQ = false;
		for (int i = 0; i < letters.length; i++) {
			final char letter = letters[i];

			if (wasQ) {
				if (letter == 'u') {
					// Return immediately we have a "qu" sequence of letters.
					return i + 1; // Return index fter "qu", not just before "u"
				}
			} else if (isVowel(letter)) {
				return i; // Return index before vowel
			}

			wasQ = letter == 'q';
		}

		return -1;
	}

	/**
	 * Translates a word from normal Latin-type language to Pig Latin.
	 *
	 * The word is chopped into two parts (usually before the vowel),
	 * and the first part is moved to the end, with a "-" prefix, and a "ay" suffix.
	 *
	 * @param word The word to translate
	 * @return The translated word
	 */
	public static final String translateWord(final String word) {
		final int firstVowel;

		/*
		 * Don't chop the word, and directly append if there was not chop point
		 */
		{
			final int tmpFirstVowel = vowelFinder(word);

			firstVowel = tmpFirstVowel < 0 ? 0 : tmpFirstVowel;
		}

		return String.format("%s-%say",
				word.substring(firstVowel), // Usually substring before vowel
				// "-"
				word.substring(0, firstVowel) // Usually substring from vowel onwards
				// "ay"
		);
	}

	/**
	 * Translates a sentence from Latin-type language to Pig Latin.
	 *
	 * @param sentence The sentence to translate
	 * @return The sentence translated to Pig Latin
	 */
	public static final String translateSentence(final String sentence) {
		final Matcher matcher = LatinLanguageUtils.WORD_PATTERN.matcher(sentence);
		final StringBuffer result = new StringBuffer();

		// Find words, and replace them
		while (matcher.find()) {
			matcher.appendReplacement(result, translateWord(matcher.group()));
		}
		matcher.appendTail(result);

		return result.toString();
	}

	private PigLatin() { throw new UnsupportedOperationException(); }
}
