import me.danielzgtg.compsci11_sem2_2017.common.appcontainer.impl.TextTransformerConsoleAppContainer;
import me.danielzgtg.compsci11_sem2_2017.common.ui.LaunchUtils;

/**
 * A command-driven command-line interface for the {@link PigLatin} translator class.
 *
 * @author Daniel Tang
 * @since 15 April 2017
 */
@SuppressWarnings("WeakerAccess")
public final class PigLatinCLI {

	/**
	 * The introduction message.
	 */
	private static final String INTRODUCTION = String.valueOf(
			PigLatin.UI_DATA.get("cli.introduction"));

	/**
	 * The goodbye message.
	 */
	private static final String GOODBYE = String.valueOf(
			PigLatin.UI_DATA.get("cli.goodbye.totranslate"));

	public static void main(final String[] args) {
		LaunchUtils.safeLaunchConsoleWrappedMainLooping(PigLatinCLI::realMain, args); // Crash prevention
	}

	public static void realMain(final String[] ignore) {
		new TextTransformerConsoleAppContainer(
				PigLatin::translateSentence,
				INTRODUCTION,
				PigLatin.translateSentence(GOODBYE)
		).launch();
	}

	private PigLatinCLI() { throw new UnsupportedOperationException(); }
}
