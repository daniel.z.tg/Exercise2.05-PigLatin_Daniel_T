import java.util.concurrent.atomic.AtomicLong;

import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.image.BufferedImage;

import me.danielzgtg.compsci11_sem2_2017.common.appcontainer.impl.TextTransformerGraphicalAppContainer;
import me.danielzgtg.compsci11_sem2_2017.common.platform.PlatformUtils;
import me.danielzgtg.compsci11_sem2_2017.common.platform.ResourceUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.GUIJAdvancedTextArea;
import me.danielzgtg.compsci11_sem2_2017.common.ui.GUIJImagePanel;
import me.danielzgtg.compsci11_sem2_2017.common.ui.GraphicsUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.SwingUtils;

/**
 * A graphical user interface (using Swing) for the {@link PigLatin} translator.
 *
 * @author Daniel Tang
 * @since 13 April 2017
 */
public final class PigLatinGUI implements Runnable {

	public static void main(final String[] args) {
		new PigLatinGUI(args.length == 0).run(); // Launch with args to prevent setting environment
	}

	/**
	 * The {@link JFrame}'s title.
	 */
	private static final String TITLE = String.valueOf(
			PigLatin.UI_DATA.get("gui.title"));

	/**
	 * The credits of the translator container.
	 */
	private static final String CREDITS = String.valueOf(
			PigLatin.UI_DATA.get("gui.credits"));

	/**
	 * Whether to set environment.
	 */
	private final boolean setupEnv;

	/**
	 * The original icon to derive other icons from.
	 */
	private static final BufferedImage ORIGINAL_ICON;

	/**
	 * The taskbar app icon.
	 */
	private static final BufferedImage TRAY_ICON;

	/**
	 * The icon that is further tinted to make the activity icon.
	 */
	private static final BufferedImage ACTIVITY_ICON_BASE;

	/**
	 * The output background.
	 */
	private static final BufferedImage OUTPUT_BACKGROUND;

	/**
	 * The input (user) text.
	 */
	private static final Font INPUT_FONT;

	/**
	 * The output (Pig Latin) text.
	 */
	private static final Font OUTPUT_FONT;

	/**
	 * The Thread that flashes the activity icon when translating.
	 */
	private static /*setonce*/ IconFlashThread iconFlashThread;

	/**
	 * Creates a new Pig Latin GUI container-container.
	 *
	 * @param setupEnv Whether to set environment.
	 */
	private PigLatinGUI(final boolean setupEnv) {
		this.setupEnv = setupEnv;
	}

	static {
		// Master icon
		ORIGINAL_ICON =
				ResourceUtils.loadBundledImgSafe("assets/image/pig/Pig.png", PigLatinGUI.class);

		// Derive these icons from master icon
		if (ORIGINAL_ICON != null) {
			// Taskbar Icon
			/*
			 * Icon should be square, and be a power of 2.
			 * Largest power of two not greater than any of provided Pig.png's dimensions is 256.
			 */
			TRAY_ICON = GraphicsUtils.smoothScaleImage(ORIGINAL_ICON, 256, 256);

			// Translation working icon
			ACTIVITY_ICON_BASE = GraphicsUtils.smoothScaleImage(
					ORIGINAL_ICON, 24, 24);
		} else {
			TRAY_ICON = null;
			ACTIVITY_ICON_BASE = null;
		}

		// Input Font; "Regular"-looking font, to indicate normal words.
		INPUT_FONT =  ResourceUtils.loadBundledTTFSafe(
				"assets/font/noto/NotoSans-Regular.ttf", PigLatinGUI.class);

		// Output Font; "Retro"-looking font, to indicate words with concealed meanings.
		OUTPUT_FONT = ResourceUtils.loadBundledTTFSafe(
				"assets/font/pressstart/PressStart2P.ttf", PigLatinGUI.class);

		// Output Background; Talking pig to indicate text is pig latin.
		OUTPUT_BACKGROUND =
				ResourceUtils.loadBundledImgSafe("assets/image/pig/PinkPig3.png", PigLatinGUI.class);
	}

	@Override
	public final void run() {
		try {
			doRun();
		} catch (final Throwable t) {
			/*
			 * Clean up the Icon Flash Thread on crash,
			 * because the callback to stop it might not be installed properly.
			 */
			if (iconFlashThread != null) {
				iconFlashThread.shutdown();
			}

			// We're not expecting an exception, so rethrow it if there is one.
			throw new RuntimeException(t);
		}
	}

	private final void doRun() {
		// Environment
		if (setupEnv) {
			PlatformUtils.setHighPerformance(true);

			if (PlatformUtils.getHighPerformance()) {
				SwingUtils.setupSystemLookAndFeel();
			}
		}

		// Last update time reference for passing to other thread
		final /*mutable*/ AtomicLong lastUpdateTimeRef = new AtomicLong();

		final TextTransformerGraphicalAppContainer container =
				new TextTransformerGraphicalAppContainer(
						PlatformUtils.getHighPerformance() ? // Use activity icon?
								// With flashing activity icon
								(sentence) -> {
									lastUpdateTimeRef.set(System.nanoTime()); // Notify Icon Flash Thread

									return PigLatin.translateSentence(sentence);
								} :
								// Without flashing activity icon
								PigLatin::translateSentence,
						// Branding Text
						TITLE, CREDITS,
						// Callback upon frame creation
						(frame) -> {
							// Branding Icon
							if (TRAY_ICON != null) {
								frame.setIconImage(TRAY_ICON);
							}

							// Cleanup Icon Flash Thread
							if (iconFlashThread != null) {
								SwingUtils.hookFrameClosing(frame, iconFlashThread::shutdown);
							}
						});

		final GUIJAdvancedTextArea outputBox = container.getOutputBox();
		if (OUTPUT_BACKGROUND != null) {
			outputBox.setImage(OUTPUT_BACKGROUND);
		}

		// Different fonts to emphasize different types of text
		if (INPUT_FONT != null) {
			SwingUtils.replaceFontFamily(container.getInputBox(), INPUT_FONT);
		}

		if (OUTPUT_FONT != null) {
			SwingUtils.replaceFontFamily(outputBox, OUTPUT_FONT);
		}

		// Setup the Flashing Activity Icon, if possible
		if (ACTIVITY_ICON_BASE != null && PlatformUtils.getHighPerformance()) {
			final GUIJImagePanel activityPanel = new GUIJImagePanel(ACTIVITY_ICON_BASE);
			final JPanel bottomPanel = container.getBottomPanel();

			// Add Activity Icon on the Left
			bottomPanel.add(activityPanel, 0);
			bottomPanel.add(Box.createRigidArea(new Dimension(10, 0)), 1); // Spacer
			// Reset of Bottom Bar (Manual Translate, Credits, Realtime Toggle, etc.) are to the Right.

			iconFlashThread = new IconFlashThread(
					lastUpdateTimeRef, activityPanel, ACTIVITY_ICON_BASE);
			iconFlashThread.start();
		}

		container.launch();
	}
}
