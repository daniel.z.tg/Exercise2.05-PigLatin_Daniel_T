import me.danielzgtg.compsci11_sem2_2017.common.StringUtils;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;

import org.junit.Test;

import static me.danielzgtg.compsci11_sem2_2017.common.ui.PrintUtils.formatln;

/**
 * Simple tests for the {@link PigLatin} translator class.
 *
 * @author Daniel Tang
 * @since 12 April 2017
 */
public final class TestingPigLatin {

	@Test
	public final void test1() {
		for (final String word : new String[] {
				"style",
				"system",
				"squeel",
				"you",
				"fly",
				"bye",
				"yttrium",
				"MSN",
				"Cianci",
				"let's"
		}) {
			testWord(word);
		}

		assertWordSimple("yttrium", "yttrium-ay");
		assertWordSimple("Cianci", "Iancicay");
		assertWordSimple("button", "utton-bay");
		assertWordSimple("star", "ar-stay");
		assertWordSimple("three", "ee-thray");
		assertWordSimple("question", "estion-quay");
		assertWordSimple("eagle", "eagle-ay");

		testSentence("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut " +
				"labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris " +
				"nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit " +
				"esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in " +
				"culpa qui officia deserunt mollit anim id est laborum.");
	}

	/**
	 * Translates a sentence to the console.
	 *
	 * @param sentence The Latin-like sentence to translate.
	 */
	private static final void testSentence(final String sentence) {
		formatln("\"%s\"\n translates to \n\"%s\"", sentence, PigLatin.translateSentence(sentence));
	}

	/**
	 * Translates a word to the console.
	 *
	 * @param word The Latin-like word to translate.
	 */
	private static final void testWord(final String word) {
		formatln("\"%20s\" translates to \"%s\"", word, PigLatin.translateWord(word));
	}

	/**
	 * Checks if a word translation matches the expected answer.
	 *
	 * @param word The word to translate.
	 * @param answer The expected answer.
	 */
	private static final void assertWordSimple(final String word, final String answer) {
		final String translated = StringUtils.asSimpleBlob(PigLatin.translateWord(word));

		Validate.require(StringUtils.asSimpleBlob(answer).equals(translated),
				"Translation Failed!");
		formatln("\"%20s\" passed test to \"%s\"", word, translated);
	}

}